---
title: Links
icon: fas fa-info
order: 5
---

This is a list of useful links, sorted by topics.

## Code Editors & IDE's

### [VSCodium](https://vscodium.com/){:target="_blank"}
- Free/Libre Open Source Software Binaries of VSCode.
- Microsoft’s vscode source code is open source (MIT-licensed), but the product available for download (Visual Studio Code) is licensed under this not-FLOSS license and contains telemetry/tracking.

### [Sublime Text](https://www.sublimetext.com/){:target="_blank"}
- A sophisticated text editor for code, markup and prose.
- Sublime Text is a shareware cross-platform source code editor with a Python application programming interface (API). It natively supports many programming languages and markup languages, and functions can be added by users with plugins, typically community-built and maintained under free-software licenses. 

---

## Graph & Plotting Tools

### [Veusz](https://veusz.github.io/){:target="_blank"}
- A scientific plotting package.
- Veusz is a scientific plotting and graphing program with a graphical user interface, designed to produce publication-ready 2D and 3D plots. In addition it can be used as a module in Python for plotting. Veusz is multiplatform, running on Windows, Linux/Unix and macOS. It supports vector and bitmap output, including PDF, Postscript, SVG and EMF. Veusz is Free Software.

### [Ipe](http://ipe.otfried.org/){:target="_blank"}
- The Ipe extensible drawing editor.
- Ipe is a drawing editor for creating figures in PDF format. It supports making small figures for inclusion into LaTeX-documents as well as making multi-page PDF presentations.

### [Matcha](https://www.mathcha.io/){:target="_blank"}
- Online Mathematics Editor.
- A fast way to write and share mathematics.

---

## LaTeX

### Nice Beamer themes
- [mtheme](https://github.com/matze/mtheme){:target="blank"}
- [cern](https://github.com/jeromebelleman/beamer-cern){:target="blank"}