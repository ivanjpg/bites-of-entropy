---
title: About
icon: fas fa-info
order: 5
---

## The Site
This site is about things I'm passionate about. Mainly you can find Physics, Computers, Math & Science stuff. Those topics are not exclusive, so I'm sure they will come out more often than others.

> Everything stated here only represents my own opinion.

## Me
I'm a Physics Ph.D. candidate in the statistical physics area, currently in the process of writting my dissertation. My research topic is confined systems diffusion, which contains the theoretical development and also computational simulations written in Fortran, C, and some supporting scripts in Ruby and Bash. I'm also an IT professional with 16 years of experience, now I'm developing mobile apps in Flutter, because, you know... third world stuff...
