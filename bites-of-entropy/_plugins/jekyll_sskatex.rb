require 'execjs'

JS_FILENAME = 'vendor/katex/katex.min.js'
MACROS = {
}

module Katex
	class << self
		JS_CTX = ::ExecJS.compile( File.read(JS_FILENAME) )

		INLINE_REGEX = [
			/\\\((.*?)\\\)/m,
			/\$(.*?)\$/m
		].freeze
		DISPLAY_REGEX = [
			/\\\[(.*?)\\\]/m,
			/\$\$(.*?)\$\$/m
		].freeze

		def process(text, macros)
			DISPLAY_REGEX.each do |regex|
				text = text.gsub(regex) { render_display($1, macros) }
			end

			INLINE_REGEX.each do |regex|
				text = text.gsub(regex) { render_inline($1, macros) }
			end

			text
		end

		def render_inline(text, macros)
			JS_CTX.call('katex.renderToString', text, macros: macros)
		end

		def render_display(text, macros)
			JS_CTX.call('katex.renderToString', text, macros: macros, displayMode: true)
		end

	end
end

pre_render_task = lambda { |doc, _|
	if doc.data['katex']
		doc.content = Katex.process(
			doc.content,
			doc.data['katex_macros'] || MACROS
		)
	end
}

Jekyll::Hooks.register(:posts, :pre_render, &pre_render_task)
Jekyll::Hooks.register(:pages, :pre_render, &pre_render_task)