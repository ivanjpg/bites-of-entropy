---
layout: post
title: 
date: 2021-11-09 08:48
category: programming
author: 
tags: [fortran,programming,kind,types,data]
summary: Fortran data-types and variable sizes are perfect examples of the Fortran version mixing (allowed by permissive compilers), correcting them is not so hard as we think.
katex: false
citedrefs: false
---

Fortran is an old programming language, having a history of 64 years at the time of writing this post, a common misconception about it is that it became outdated and useless, despite the latest specification being dated 2018.

The reliability and speed are their main advantages of Fortran even compared with C. In scientific applications we need to focus on the science, on the results, on this matter, C gives us a little more struggle because more attention to details is needed, e.g., memory management, pointers, structures… If you ever did C you already know what I mean, do you remember to have a _Segmentation Fault_ by writing a ten-line code? That is my point. Of course, sometimes you will need other capabilities which only can get using C, but if you are doing scientific computing, that is unusual.

Inside academia, you can find a lot of researchers and students who use a weird/strange/creepy mix of Fortran flavors and versions. Some teachers still code with Fortran 77 fixed-form and syntax, but combining with modern Fortran 90 declarations, types, and _intrinsics_, also non-standard extensions, e.g., GNU or Intel. Of course, most of them (us) are not trained as programmers, instead, we learn as needed, being supported by really permissible and backward-compatible compilers. Nevertheless, none of these common activities are considered good practices and must be amended.

It is customary that imperative and strongly-typed programming languages (like Fortran) require that variables be declared by specifying their data type which is related to the memory space reserved and used by them. To do calculations, computers store numbers in certain ways that usually does not get messy for integer operations, but floating point is a different story, and to get more accuracy we need more space which sometimes also means lost of performance (but sometimes a weird mix of precisions can lead to some speed improvement, see [here](https://arxiv.org/abs/2107.03357v1){:target='_blank'}).

## Old School

A common way to declare variable precision is the _star notation_, a non-standard form that specifies the data type (numerical for our purposes), and the number of bytes that is supposed to use inside the computer memory. A declaration like `real*8` is expected to reserve 8 bytes to store a floating-point number, but this does not guarantee a certain **precision** between different systems, so this is not portable code even if it works as you expect in your current equipment. An equivalent non-portable way to declare variables is by the `kind` word doing something like `real(kind=4)` which in most cases will mean that the variable will be stored in 4-byte memory space, but it is not guaranteed.

## New and Standard Ways

A portable standardized way to make variable declarations is using the intrinsic functions `select_XXX_kind([P,R,RADIX])` where _XXX_ stands for char, integer, and real. For char variables are somewhat different, but the idea is the same for integer and real. We can specify two or three parameters depending on the compiler version, we can work out with two which are present since Fortran 90, for three parameters we need a Fortran 2008 compiler. The first parameter, _P_, is the decimal precision, how many numbers we want to consider significant. The second argument, _R_, refers to exponent range. Both parameters are specifying as a lower bound, this is, we are requesting a decimal precision of **at least** _P_ digits, and an exponent range of **at least** _R_, but we can get higher precision and exponent range kinds. Here is an example

```fortran
program some_types

implicit none

! selected_XXX_kind(...) functions are available since Fortran 90
! and can receive two parameters (F90): selected_real_kind(P, R)
!       p -> Decimal precision of at least P digits
!       r -> Exponent range of at least R
integer, parameter :: rs32 = selected_real_kind(6)
integer, parameter :: rs64 = selected_real_kind(15)
integer, parameter :: rs128 = selected_real_kind(30)

real(kind=rs32) :: single_precision
real(kind=rs64) :: double_precision
real(kind=rs128) :: quadruple_precision

...
...
...

end program some_types
```

We can take a look at the [Fortran manual](https://gcc.gnu.org/onlinedocs/gfortran/SELECTED_005fREAL_005fKIND.html){:target='_blank'} and find that these functions return negative values if some of the programmer requirements cannot be fulfilled. Another advantage of this approach relies on the fact that the change of precision in the whole program becomes a really easy task, you only need to change the parameters of the `select_XXX_kind(...)` functions in order to achieve it, without tedious and repetitive replacements of data types.

Now, there is another approach to this portable and system-independent way to specify the precision of the numerical variables. Since Fortran 2003 there is an intrinsic module called `iso_fortran_env` which already contains the definitions of the integer, real, and char kinds. It can be used like this

```fortran
program some_types2

! Load the module and import only the parameters we need
use, intrinsic :: iso_fortran_env, &
            only: ru32 => real32, &
                  ru64 => real64, &
                  ru128 => real128

implicit none

real(kind=ru32) :: single_precision
real(kind=ru64) :: double_precision
real(kind=ru128) :: quadruple_precision

...
...
...

end program some_types2
```

If we take a look at the [NAG Fortran Manual](https://www.nag.com/nagware/np/r61_doc/iso_fortran_env.html){:target="_blank"} it is found an interesting fact: the parameters are defined using `selected_XXX_kind(...)` intrinsics! As an example, we can see that `real64` is defined as

```fortran
INTEGER,PARAMETER :: real64 = SELECTED_REAL_KIND(15)
```

I know, it is hard to fight against the old habits but as you can see, none of the two forms of specifying the precision/range/radix for numerical variables are too tough to learn.  