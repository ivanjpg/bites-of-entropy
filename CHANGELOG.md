# Changelog

## 29.03.2021
### Changed
- Now the post list shows the `summary` field if it exists, instead of showing an excerpt of 200 chars.

---

## 29.03.2021
### Added
- Katex from the server side.
- `jekyll-scholar` for references management.
### Changed
- Using the Ruby gem of the theme instead the github repos fork.

---

## 30.10.2020
### Added
- Added chirpy theme.
- `CHANGELOG.md` created.
- Linkedin added to sharing options in `share.yml`.
### Changed
- `.gitlab-ci.yml` modified to reflect the directory structure.

---
