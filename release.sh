#!/usr/bin/env bash

BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

cd ${BASEDIR}

pushd bites-of-entropy
JEKYLL_ENV=production bundle exec jekyll b
popd

git add .
git commit -m "Site update: `date '+%Y%m%d-%H%M%S'`"
git push
